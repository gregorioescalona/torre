import React from 'react'
import {FavoriteJobModel} from '../../core/models';
import { JobCardDetailView } from '../JobsComponents';
import {useTorreSearchJobById} from '../../hooks/useTorreSearchJobById';
interface FavoriteJobsListProps {
    jobsList: FavoriteJobModel[];
}

export const FavoriteJobsList:React.FC<FavoriteJobsListProps>=({jobsList})=>{

    const showCarList = (jobId) =>{
        const{ jobs, isLoading, isError } = useTorreSearchJobById(jobId);
        return jobs;
    }
    return (
        <div className="bg-gray-900">
            <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-24">
                <div className="space-y-12">
                <div className="space-y-5 sm:space-y-4 md:max-w-xl lg:max-w-3xl xl:max-w-none">
                    <h2 className="text-3xl font-extrabold text-white tracking-tight sm:text-4xl">My favorite jobs</h2>
                    <p className="text-xl text-gray-300">
                        You said that this are you favorite jobs.
                    </p>
                </div>
                <ul role="list" className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8">
                    {
                        jobsList?.map((favoriteJob)=>{

                            let favoriteJobFromTorre = showCarList(favoriteJob.torre_job_id);
                            return (<JobCardDetailView key={favoriteJob.torre_job_id} job={favoriteJobFromTorre} />)
                        })
                    }
                </ul>
                </div>
            </div>
        </div>
    )
}
