import React from 'react'
import { Loading } from '@nextui-org/react';
import {Tooltip} from '@nextui-org/react';
import { useRouter } from 'next/router'


export const JobCardDetailView = ({job}) => {
    if(job === undefined) return <Loading  type="points" />
    
    const router = useRouter();
    
    const goToTorre =(torreId: string, torre_slug: string)=>{

        router.push(
        `https://torre.co/post/${torreId}-${torre_slug}?utm_source=web&utm_medium=srh_jobs&utm_campaign=dft&utm_term=undefined&utm_content=card`);
    }
    return (
        <div className="container">
      <div className="max-w-md w-full bg-gray-800 shadow-lg rounded-xl p-6">
        <div className="flex flex-col ">
          <div className="">
            <div className="relative h-62 w-full mb-3">
              
              <img src={job.organizations[0].picture} alt={job.organizations[0].name} className=" w-full   object-fill  rounded-2xl" />
            </div>
            <div className="flex-auto justify-evenly">
              <div className="flex flex-wrap ">
                <div className="w-full flex-none text-sm flex items-center text-gray-600">
                  <svg xmlns="http://www.w3.org/2000/svg" className="h-4 w-4 text-red-500 mr-1" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                  </svg>
                  <span className="text-gray-400 whitespace-nowrap mr-3">4.60</span><span className="mr-2 text-gray-400">India</span>
                </div>
                <div className="flex items-center w-full justify-between min-w-0 ">
                    <h2 className="text-lg mr-auto cursor-pointer text-gray-200 hover:text-purple-500 truncate ">
                            {job.objective}
                    </h2>
                  <div className="flex items-center bg-green-400 text-white text-xs px-2 py-1 ml-3 rounded-lg">
                    <Tooltip text={job.objective} color="warning">{job.status}</Tooltip>
                    </div>
                </div>
              </div>
              <div className="text-xl text-white font-semibold mt-1">{job.serpTags.baseSalary.currency} {job.serpTags.baseSalary.value.minValue} - {job.serpTags.baseSalary.value.maxValue}</div>
              <div className="lg:flex  py-2 text-sm text-gray-600">
                <div className="flex-1 inline-flex items-center  mb-3">
                  <div className="w-full flex-none text-sm flex items-center text-gray-600">
                    <ul className="flex flex-row justify-center items-center space-x-2">
                        {job.members.map((member, index) => {
                            if(index > 9) return
                            return (
                                <li className="">
                                    <span className="block p-1 border-2 border-gray-900 hover:border-blue-600 rounded-full transition ease-in duration-300">
                                        <Tooltip text={member.person.name} color="warning">
                                            <img src={member.person.picture} className="block w-3 h-3 bg-blue-600 rounded-full" />
                                        </Tooltip>
                                    </span>
                                </li>
                            )
                        })}
                    </ul>
                  </div>
                </div>
              </div>
              <div className="flex space-x-2 text-sm font-medium justify-start">
                <button
                    onClick={()=>goToTorre(job.id, job.slug)}
                    className="transition ease-in duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-purple-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-purple-600 ">
                  <span>GO TO TORRE</span>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    )
}
