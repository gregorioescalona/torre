import React, {useState} from 'react'
import {JobCardCardModel} from '../../core/models';
import { TorreModal } from '../Common/TorreModal';
import Image from 'next/image';
import { Tooltip } from '@nextui-org/react';
import {useAddToFavorite} from '../../hooks/useAddToFavorite';
import {addToFavoriteBE} from '../../core/apis';
interface JobCard {
    job: JobCardCardModel;
}
export const JobCardView: React.FC<JobCard> =({
    job
}) => {
    const [loadingFavorite, setLoadingFavorite] = useState(false);
    const [open, setOpen] = useState(false);
      const addToFavorite = async () => {
      const jobData = {
         "users_permissions_user": {id: 1}, // The user should logging
        "title": job.objective,
        "url": job.objective,
        "close_date": "2021-09-19T10:42:39.382Z",
        "skills_required": job.skills,
        "compensations": job.compensation,
        "slug": job.slug,
        "torre_job_id": job.id,
        "published_at": new Date(),
      }
      
      const {favorite,isLoading, status} = await useAddToFavorite(jobData);
      
      if(status === 200) setOpen(!open);

    }
    return (
      <li className="py-10 px-6 bg-gray-800 text-center rounded-lg xl:px-10 xl:text-left animated fadeIn faster left-0 top-0">
          <div className="space-y-6 xl:space-y-10">
            <img className="mx-auto h-40 w-40 rounded-full xl:w-56 xl:h-56" 
            src={job.organizations[0].picture} alt="" />
            <div className="flex flex-col  space-y-2 xl:flex xl:items-center xl:justify-between">
             
              <div className="font-medium text-lg leading-6 space-y-1">
                <h3 className="text-white text-sm">
                  {job.objective}
                </h3>
                <ul className="grid grid-cols-3 space-betwee w-full">
                  {job.skills.map((skill) => {
                    return (
                      <li className="ml-2 inline"><p className="text-xs text-indigo-400 inl">{skill.name}</p></li>
                    )
                  })}
                </ul>
              </div>
              {  (job?.compensation.data === null || job?.compensation.data === undefined)? null : 
                <div className="text-lg text-yellow-500">
                  {job?.compensation?.data?.currency}{' '}{job?.compensation?.data?.periodicity} {job?.compensation?.data?.maxAmount} 
                </div>
              }
                
              
              <ul role="list" className="flex justify-center space-x-5">
                {job.members.map((member, i)=>{
                  if(i >= 5) return;
                  return(
                    <li key={i}>
                      <a href="#" className="text-gray-400 hover:text-gray-300">
                        <span className="sr-only">{member?.name}</span>
                        <Tooltip  text={member?.name}>
                          <Image src={ member.picture? member.picture : job.organizations[0].picture} alt={member?.name} width="30" height="30" className="rounded-full" />
                        </Tooltip>
                      </a>
                    </li>
                  )
                })}
              </ul>
            </div>
            <div className="flex space-x-2 text-sm font-medium justify-start">
                <button
                    onClick={addToFavorite}
                    className="transition ease-in duration-300 inline-flex items-center text-sm font-medium mb-2 md:mb-0 bg-purple-500 px-5 py-2 hover:shadow-lg tracking-wider text-white rounded-full hover:bg-purple-600 ">
                  <span>Add to favorite</span>
                </button>
              </div>
          </div>
          <TorreModal isOpen={open} openModal={setOpen} jobName={job.objective} />
        </li>

    )
}
