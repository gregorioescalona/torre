import React from 'react'
import { NextPage } from 'next';
import {TorreFavoritesJobsContainer} from '../containers/TorreFavoritesJobsContainer';
const FavoritesJobs: NextPage<{}> = ()=> {
    return (
        <TorreFavoritesJobsContainer />
    )
}

export default FavoritesJobs;