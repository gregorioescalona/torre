import Head from 'next/head'
import { getSortedPostsData } from '../lib/posts'
import styles from '../styles/Home.module.css'
import {TorreJobsListContainer} from '../containers/TorreJobsListContainer';
import { Container } from '@nextui-org/react'

export default function Home({ allPostsData }) {
  return (
     <TorreJobsListContainer />
  )
}

export async function getStaticProps() {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}
