import useSWR from 'swr';
import { TORRE_OPPORTUNITIES_SEARCH } from '../core/constants';
import axios from 'axios'

const fetcher = url => axios.post(url).then(res => res.data)

export const useTorreSearch = () => {
  const { data, error } = useSWR(`${TORRE_OPPORTUNITIES_SEARCH}/_search?currency=USD%24&periodicity=hourly&lang=en&size=20&aggregate=false`, fetcher);

  return {
    jobs: data,
    isLoading: !error && !data,
    isError: error
  }
}