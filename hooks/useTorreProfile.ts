import useSWR from 'swr';
import { USER_BIO } from '../core/constants';
import axios from 'axios'

const fetcher = url => axios.get(url).then(res => res.data)

export const useTorreProfile = (username) => {
  const { data, error } = useSWR(USER_BIO.replace(':username', username ), fetcher);

  return {
    user: data,
    isLoading: !error && !data,
    isError: error
  }
}