import { TORRE_FAVORITE_JOBS } from '../core/constants';
import axios from 'axios'


const headers = {
  'Content-Type': 'application/json',
  'Authorization': `Bearer ${process.env.NEXT_PUBLIC_LOCAL_API_TOKEN}`
}

export  const useAddToFavorite = async(jobData) => {

    const data = axios.post(TORRE_FAVORITE_JOBS, jobData, {headers});
    const favorite = await data.then(res => res.data);
    const status = await data.then(res => res.status);
    const error = await data.catch(err =>err);
    
    return {
        favorite: favorite,
        isLoading: !error && !data,
        status: status
    }
}