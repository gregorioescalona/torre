import useSWR from 'swr';
import { TORRE_FAVORITE_JOBS } from '../core/constants';
import axios from 'axios'
const headers = {
  'Content-Type': 'application/json',
  'Authorization': `Bearer ${process.env.NEXT_PUBLIC_LOCAL_API_TOKEN}`
}

const fetcher = url => axios.get(url, {
  headers
}).then(res => res.data)

export const useTorreFavorite = () => {
  const { data, error } = useSWR(`${TORRE_FAVORITE_JOBS}`, fetcher);

  return {
    jobs: data,
    isLoading: !error && !data,
    isError: error
  }
}