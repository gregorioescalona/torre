import useSWR from 'swr'
import { JOBS_ENDPOINT } from '../core/constants';
import axios from 'axios'

const fetcher = url => axios.get(url).then(res => res.data)
export const useTorreJobs =()=> {

    console.log(JOBS_ENDPOINT);
    console.log("CUSTON HOOKS");
    const { data, error } = useSWR(`${JOBS_ENDPOINT}`, fetcher)

    return {
        jobs: data,
        isLoading: !error && !data,
        isError: error
    }
}
