import useSWR from 'swr';
import { TORRE_OPPORTUNITIES } from '../core/constants';
import axios from 'axios'

const fetcher = url => axios.get(url).then(res => res.data)

export const useTorreSearchJobById = (id: string) => {
  const { data, error } = useSWR(`${TORRE_OPPORTUNITIES}/${id}`, fetcher);

  return {
    jobs: data,
    isLoading: !error && !data,
    isError: error
  }
}