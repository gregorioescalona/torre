import React from 'react'
import { useTorreSearch } from '../hooks/useTorreSearch';
import { Spacer } from '@nextui-org/react'
import { Loading } from '@nextui-org/react';
import {JobCardView} from '../components/JobsComponents';

export const TorreJobsListContainer = () => {
    const{ jobs, isLoading, isError } =  useTorreSearch();
    
    if (isLoading) return <Loading type="points" />
    if (isError) return <h1>Error..</h1>


    return (
      <div className="bg-gray-900">
        <div className="mx-auto py-12 px-4 max-w-7xl sm:px-6 lg:px-8 lg:py-24">
          <div className="space-y-12">
            <div className="space-y-5 sm:space-y-4 md:max-w-xl lg:max-w-3xl xl:max-w-none">
              <h2 className="text-3xl font-extrabold text-white tracking-tight sm:text-4xl">Your dream job is here</h2>
              <p className="text-xl text-gray-300">
                You can find the job of your dreams with us.
              </p>
            </div>
            <ul role="list" className="space-y-4 sm:grid sm:grid-cols-2 sm:gap-6 sm:space-y-0 lg:grid-cols-3 lg:gap-8">
                {
                  jobs.results.map((job) =>{
                    return (
                      <JobCardView key={job.id} job={job}/>
                    )
                  })
                }
            </ul>
          </div>
        </div>
      </div>
    )
}
