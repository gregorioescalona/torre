import React from 'react'
import {FavoriteJobsList} from '../components/FavoriteJobs/FavoriteJobsList';
import { useTorreFavorite } from '../hooks/useTorreFavorite';
import { Loading } from '@nextui-org/react';

export const TorreFavoritesJobsContainer = () => {
     const {jobs, isLoading, isError } = useTorreFavorite();

     if (isLoading) return <Loading type="points" />
    if (isError) return <h1>Error..</h1>

    return <FavoriteJobsList jobsList={jobs} />
}

