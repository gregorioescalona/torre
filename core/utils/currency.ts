import DineroJS from 'dinero.js';
import first from 'lodash/first';

export type DineroType = DineroJS.Dinero;
type DineroOptions = DineroJS.Options;

export const Dinero = (amount: number, options?: DineroOptions): DineroType => {
  return DineroJS({ amount, currency: 'USD', precision: 2, ...options });
};

export const displayPrice = (price: number, inCents = true, showFractions = true): string => {
  const dineroFormat = Dinero(price, { precision: inCents ? 2 : 0 }).toFormat();

  return showFractions ? dineroFormat : first(dineroFormat.split('.')) || '';
};