
export class SkillModel {
    experience: string;
    name:  string;
    proficiency:  string;
}

export class OrganizationModel {
    id: number;
    name: string;
    picture: string;
}
export class MemberModel{
    manager: boolean;
    member: boolean;
    name: string;
    picture: string;
    poster: boolean;
    professionalHeadline: string;
    subjectId: string;
    username: string;
    weight: number;
}
export class CompensationModel {
    data: {
        code: string;
        currency: string;
        maxAmount: number;
        maxHourlyUSD: number;
        minAmount: number;
        minHourlyUSD: number;
        periodicity: string;
    }
}
export class JobCardCardModel {
    objective: string;
    locations: [] | null;
    slug: string;
    id: string
    skills: SkillModel[] | null;
    created: Date | null;;
    deadline: Date | null;
    organizations: OrganizationModel[] | null;
    members: MemberModel[] | null;
    compensation: CompensationModel;

}

export class FavoriteJobModel {
    id: string;
    users_permissions_user: {
        id: string;
        username: string;
        email: string;
        provider: string;
        password: string;
        resetPasswordToken: string;
        confirmationToken: string;
        confirmed: true;
        blocked: true;
        role: string;
        torrejobs: [
            string
        ];
        created_by: string;
        updated_by: string
    };
    title: string;
    url: string;
    close_date: Date;
    skills_required: {};
    compensations: {};
    slug: string;
    torre_job_id: string;
    published_at: Date;
}