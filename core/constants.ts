export const BASE_URL = `${process.env.NEXT_PUBLIC_TORRE_API_V1_URL_BASE}`;
export const BASE_LOCAL_URL = `${process.env.NEXT_PUBLIC_LOCAL_API}`;
export const BASE_OPPORTUNITIES_URL = `${process.env.NEXT_PUBLIC_TORRE_OPPOTUNITIES_API_V1_URL_BASE}`;

export const BASE_SEARCH_URL = `${process.env.NEXT_PUBLIC_TORRE_SEARCH_API_V1_URL_BASE}`;
export const  JOBS_ENDPOINT = `${BASE_URL}/opportunities/_search?currency=USD`
export const  USER_BIO = `${BASE_URL}/bios/:username`
export const  TORRE_OPPORTUNITIES = `${BASE_OPPORTUNITIES_URL}/suite/opportunities`
export const  TORRE_OPPORTUNITIES_SEARCH = `${BASE_SEARCH_URL}/opportunities`
export const  TORRE_FAVORITE_JOBS = `${BASE_LOCAL_URL}/torrejobs`

